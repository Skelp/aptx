import torch
import torch.nn as nn

class APTx(nn.Module):
    def __init__(self, alpha=1.0, beta=1.0, gamma=0.5):
        super(APTx, self).__init__()
        self.alpha = alpha
        self.beta = beta
        self.gamma = gamma

    def forward(self, x):
        tanh_beta_x = torch.tanh(self.beta * x)
        phi_x = (self.alpha + tanh_beta_x) * self.gamma * x
        return phi_x

    def extra_repr(self):
        return 'alpha={}, beta={}, gamma={}'.format(self.alpha, self.beta, self.gamma)