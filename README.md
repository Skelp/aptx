# APTx Activation Function Implementation

This repository contains the PyTorch implementation of the APTx activation function, as proposed in the research paper ["APTx: Better Activation Function than MISH, SWISH, and ReLU's Variants Used in Deep Learning"](https://arxiv.org/abs/2209.06119). APTx aims to introduce an efficient non-linearity to deep neural networks, outperforming existing activation functions like MISH, SWISH, and various ReLU variants with fewer mathematical operations, thereby speeding up model training and reducing hardware requirements.

## Overview

The `APTx` activation function is designed to provide an optimal balance between computational efficiency and the ability to help deep neural networks learn faster and more efficiently. According to the paper, APTx requires lesser computational resources compared to its predecessors, making it an attractive option for deep learning applications.
Simple performance tests show this to be the case when benchmarked against the pytorch functional implementation of MISH.

![APT-X Plot](aptx_plot.png)

## Repository Structure

This repository contains two PyTorch script files:

- `aptx.py`: Contains the regular implementation of the APTx activation function.
- `aptxp.py`: Contains a similar implementation but with trainable parameters for alpha, beta, and gamma, allowing for further optimization during the training process.

Both implementations define a class named `APTx`, with the latter allowing dynamic adjustment of the activation function's parameters.

## Installation

To use the APTx activation function in your PyTorch models, clone this repository to your local machine:

```bash
git clone https://gitlab.com/Skelp/aptx.git
```

Ensure you have PyTorch installed in your environment. If not, you can install PyTorch by following the instructions on the [official PyTorch website](https://pytorch.org/get-started/locally/).

## Usage

### Regular Implementation

To use the regular implementation of APTx in your PyTorch models, import the `APTx` class from `aptx.py`:

```python
from aptx import APTx

activation_function = APTx(alpha=1.0, beta=1.0, gamma=0.5)
```

### Trainable Parameters Implementation

To utilize the APTx implementation with trainable parameters, import the `APTx` class from `aptxp.py`:

```python
from aptxp import APTx

activation_function = APTx(alpha=1.0, beta=1.0, gamma=0.5)
```

In this version, the parameters `alpha`, `beta`, and `gamma` can be optimized during the training process.

## License

This project is open-sourced under the MIT license. See the LICENSE file for more details.

## Acknowledgments

This implementation is based on the research paper linked above. We encourage users to refer to the original paper for a detailed understanding of the APTx activation function and its advantages.